import 'package:flutter/material.dart';
import 'package:flutter_application_1/pages/payment.dart';
import 'package:provider/provider.dart';

import '../models/Transaction.dart';
import '../providers/transaction_provider.dart';

class Booking extends StatelessWidget {
  final formKey = GlobalKey<FormState>();

  //controller
  final titleController = TextEditingController(); //รับค่าชื่อรายการ
  final amountController = TextEditingController(); //รับค่าตัวเลขจำนวนเงิน
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Booking"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextFormField(
                  decoration: new InputDecoration(labelText: "Name."),
                  autofocus: true,
                  controller: titleController,
                  validator: (str) {
                    if (str!.isEmpty) {
                      return "Please enter your name.";
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 10),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                  child: TextField(
                    decoration: InputDecoration(hintText: 'Time.'),
                  ),
                ),
                Text(
                  '',
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
                ElevatedButton(
                  child: Text("Confirm."),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return payment();
                    }));
                  },
                ),
                Text(
                  '',
                  style: Theme.of(context).textTheme.headline1,
                ),
                Text(
                  'SERVICE RATE.',
                  style: Theme.of(context).textTheme.headline6,
                ),
                Text(
                  '',
                  style: Theme.of(context).textTheme.headline5,
                ),
                Text(
                  'Guest: 400-600    Baht/Hour.               ',
                  style: Theme.of(context).textTheme.headline6,
                ),
                Text(
                  '',
                  style: Theme.of(context).textTheme.bodySmall,
                ),
                Text(
                  'Before : 6:00 P.M pay 400 Baht/Hour.',
                  style: Theme.of(context).textTheme.headline6,
                ),
                Text(
                  'After : 6:00 P.M pay 600 Baht/Hour.   ',
                  style: Theme.of(context).textTheme.headline6,
                ),
              ],
            ),
          ),
        ));
  }
}
